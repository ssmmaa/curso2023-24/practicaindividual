[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Descripción

Desde el Ayuntamiento de Jaén se está implementando un plan de movilidad sostenible con el uso de 
bicicletas compartidas entre sus ciudadanos mediante el uso de unas estaciones de servicio 
ubicadas en diferentes zonas de la ciudad. Antes de la implantación definitiva quieren comprobar
mediante una simulación con agentes las fortalezas y debilidades del sistema.

## Primera Práctica

Como primer paso se va a implementar un agente que representará a una estación de bicicletas que
mostrará el estado en que se encuentra cada una de ellas.

Implementar AgenteEstacionBicicletas para llevar el control de las bicicletas que se encuentran
a su cargo. El agente deberá saber en todo momento las bicicletas que están en la estación y los
huecos libres. De las bicicletas que se encuentran en la estación deberá mostrar su estado.

### Tareas a implementar

Las tareas que se tienen que implementar solo servirán para que el alumno tenga un primer contacto
con la implementación de agentes y compruebe el funcionamiento del ciclo de vida del agente

#### Inicialización de la estación
_Disponible_: al inicio del agente

_Acción_ :
```
espacio = generarAleatorio(MIN_ESPACIO,MAX_EXPACIO);
bicicletas = generarAleatorio(espacio)
inicializarEstacion(espacio,bicicletas)
inicializarGUI()
```

_Finalización_ : ``true``

### Operaciones de la estación
_Disponible_ : a la finalización de la inicialización de la estación

_Acción_:
```
accion = generarAleatoro(D100)
decidirAccion(ListaAcciones,accion)
```

_Finalizacion_ : Después de ``NUM_ACCIONES``

## Evaluación de la práctica

### Práctica entregada(Aprobado)
Debe ejecutar sin errores y debe mostrar el estado de la estación en un área de texto.
En el área de texto deberá mostrarse el estado de los huecos disponibles o el estado de
las bicicletas dentro de la estación.

### Práctica Notable
Además la interface de la estación deberá mostrar la información utilizando diferentes
elementos gráficos que mejoren la presentación de información del estado de la estación

### Práctica Sobresaliente
La inicialización deberá tener un formulario para que el usuario pueda configurar la 
estación con los parámetros que se consideren más importantes. A la finalización se mostrará 
un cuadro de diálogo con la última información de la estación antes de eliminar la interfaz.