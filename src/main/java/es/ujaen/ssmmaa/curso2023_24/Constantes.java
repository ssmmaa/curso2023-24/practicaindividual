package es.ujaen.ssmmaa.curso2023_24;

import java.util.Random;

public interface Constantes {
    Random aleatorio = new Random();
    enum EstadoBicicleta {
        DISPONIBLE, ALQUILADA, EN_TRANSITO, EN_REPARACION, FUERA_DE_SERVICIO, MANTENIMIENTO;
    }

    enum PlazaEstacionamiento {
        LIBRE, OCUPADA;
    }

    enum ListaAcciones {
        ALQUILAR, RECOGER, DEVOLVER, REPARAR;
    }
}
